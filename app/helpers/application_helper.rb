module ApplicationHelper

  def copyright_generator
    DevcampViewTool::Renderer.copyright 'Divine Touch', 'All rights reserved'
  end
end

module DevcampViewTool
  class Renderer
    def self.copyright name, msg
      "&copy; #{Time.now.year} | <b>#{name}</b> #{msg}".html_safe
    end
  end
end
